/**
 * 
 */
package es.eug.cfgs.entorns.edifici.subpaquete;

/**
 * @author Alex
 *
 */
public class Edifici {
	private Planta Planta1, Planta2, Planta3;
	private Parking parking1;

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */

	/**
	 * @param planta1
	 * @param planta2
	 * @param planta3
	 */
	public Edifici(Planta planta1, Planta planta2, Planta planta3, Parking parking1) {
		super();
		this.Planta1 = planta1;
		this.Planta2 = planta2;
		this.Planta3 = planta3;
		this.parking1 = parking1;
	}

	public Edifici(int p1h1dormitoris, boolean p1h1calefaccio, float p1h1m2, int p1h2dormitoris, boolean p1h2calefaccio,
			float p1h2m2, int p1h3dormitoris, boolean p1h3calefaccio, float p1h3m2, int p2h1dormitoris,
			boolean p2h1calefaccio, float p2h1m2, int p2h2dormitoris, boolean p2h2calefaccio, float p2h2m2,
			int p2h3dormitoris, boolean p2h3calefaccio, float p2h3m2, int p3h1dormitoris, boolean p3h1calefaccio,
			float p3h1m2, int p3h2dormitoris, boolean p3h2calefaccio, float p3h2m2, int p3h3dormitoris,
			boolean p3h3calefaccio, float p3h3m2, int placesParking, float m2_parking) {

		Planta1 = new Planta(p1h1dormitoris, p1h1calefaccio, p1h1m2, p1h2dormitoris, p1h2calefaccio, p1h2m2,
				p1h3dormitoris, p1h3calefaccio, p1h3m2);
		Planta2 = new Planta(p2h1dormitoris, p2h1calefaccio, p2h1m2, p2h2dormitoris, p2h2calefaccio, p2h2m2,
				p2h3dormitoris, p2h3calefaccio, p2h3m2);
		Planta3 = new Planta(p3h1dormitoris, p3h1calefaccio, p3h1m2, p3h2dormitoris, p3h2calefaccio, p3h2m2,
				p3h3dormitoris, p3h3calefaccio, p3h3m2);
		parking1 = new Parking( placesParking,  m2_parking);
	}

	/**
	 * @return the planta1
	 */
	public Planta getPlanta1() {
		return Planta1;
	}

	/**
	 * @return the planta2
	 */
	public Planta getPlanta2() {
		return Planta2;
	}

	/**
	 * @return the planta3
	 */
	public Planta getPlanta3() {
		return Planta3;
	}

	/**
	 * @return the parking1
	 */
	public Parking getParking1() {
		return parking1;
	}

	/**
	 * @param planta1 the planta1 to set
	 */
	public void setPlanta1(Planta planta1) {
		Planta1 = planta1;
	}

	/**
	 * @param planta2 the planta2 to set
	 */
	public void setPlanta2(Planta planta2) {
		Planta2 = planta2;
	}

	/**
	 * @param planta3 the planta3 to set
	 */
	public void setPlanta3(Planta planta3) {
		Planta3 = planta3;
	}

	/**
	 * @param parking1 the parking1 to set
	 */
	public void setParking1(Parking parking1) {
		this.parking1 = parking1;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Edifici \nplanta1" + Planta1 + "\nPlanta2=" + Planta2 + "\nPlanta3=" + Planta3 + "\nparking ="
				+ parking1;
	}

}
