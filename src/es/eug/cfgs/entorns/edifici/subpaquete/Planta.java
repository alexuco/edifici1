/**
 * 
 */
package es.eug.cfgs.entorns.edifici.subpaquete;

import es.eug.cfgs.entorns.edifici.subpaquete.Habitatge;

/**
 * @author Alex
 * @param <NumDormitoris>
 * @param <CalefaccioHabitatge>
 *
 */
public class Planta {
	private Habitatge habitatge1, habitatge2, habitatge3;

	/**
	 * @param habitatge1
	 * @param habitatge2
	 * @param habitatge3
	 */
	public Planta(Habitatge habitatge1, Habitatge habitatge2, Habitatge habitatge3) {
		super();
		this.habitatge1 = habitatge1;
		this.habitatge2 = habitatge2;
		this.habitatge3 = habitatge3;
	}

	/**
	 * 
	 * @param h1dormitoris
	 * @param h1calefaccio
	 * @param h1m2
	 * @param h2dormitoris
	 * @param h2calefaccio
	 * @param h2m2
	 * @param h3dormitoris
	 * @param h3calefaccio
	 * @param h3m2
	 */
	public Planta(int h1dormitoris, boolean h1calefaccio, float h1m2, int h2dormitoris, boolean h2calefaccio,
			float h2m2, int h3dormitoris, boolean h3calefaccio, float h3m2) {
		this.habitatge1 = new Habitatge(h1dormitoris, h1calefaccio, h1m2);
		this.habitatge2 = new Habitatge(h2dormitoris, h2calefaccio, h2m2);
		this.habitatge3 = new Habitatge(h3dormitoris, h3calefaccio, h3m2);
	}

	/**
	 * @return the habitatge1
	 */
	public Habitatge getHabitatge1() {
		return habitatge1;
	}

	/**
	 * @return the habitatge2
	 */
	public Habitatge getHabitatge2() {
		return habitatge2;
	}

	/**
	 * @return the habitatge3
	 */
	public Habitatge getHabitatge3() {
		return habitatge3;
	}

	/**
	 * @param habitatge1
	 *            the habitatge1 to set
	 */
	public void setHabitatge1(Habitatge habitatge1) {
		this.habitatge1 = habitatge1;
	}

	/**
	 * @param habitatge2
	 *            the habitatge2 to set
	 */
	public void setHabitatge2(Habitatge habitatge2) {
		this.habitatge2 = habitatge2;
	}

	/**
	 * @param habitatge3
	 *            the habitatge3 to set
	 */
	public void setHabitatge3(Habitatge habitatge3) {
		this.habitatge3 = habitatge3;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Planta \nhabitatge1=" + habitatge1 + "\nhabitatge2=" + habitatge2 + "\nhabitatge3=" + habitatge3;
				
	}

}




