/**
 * 
 */
package es.eug.cfgs.entorns.edifici.subpaquete;

/**
 * @author Alex
 *
 */
public class Habitatge {
	private int dormitoris;
	private boolean calefaccio;
	private float m2;
	
	/**
	 * @param dormitoris
	 * @param calefaccio
	 * @param m2
	 */
	public Habitatge(int dormitoris, boolean calefaccio, float m2) {
		super();
		this.dormitoris = dormitoris;
		this.calefaccio = calefaccio;
		this.m2 = m2;
	}
	/**
	 * 
	 * @return retorna un in de nombre de dormitoris
	 */
	public int getDormitoris() {
		return dormitoris;
	}
	public void setDormitoris(int dormitoris) {
		this.dormitoris = dormitoris;
	}
	public boolean isCalefaccio() {
		return calefaccio;
	}
	public void setCalefaccio(boolean calefaccio) {
		this.calefaccio = calefaccio;
	}
	public float getM2() {
		return m2;
	}
	public void setM2(float m2) {
		this.m2 = m2;
	}

	@Override
	public String toString() {
		return "habitatge dormitoris=" + dormitoris + "\ncalefaccio=" + calefaccio + "\nm2=" + m2;
	}
	

}
