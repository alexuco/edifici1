/**
 * 
 */
package es.eug.cfgs.entorns.edifici.subpaquete;

/**
 * @author Alex
 *
 */
public class Parking {
	private int placesParking;
	private float m2_parking;
	
	/**
	 * @param placesParking
	 * @param m2_parking
	 */
	public Parking(int placesParking, float m2_parking) {
		super();
		this.placesParking = placesParking;
		this.m2_parking = m2_parking;
	}
	/**
	 * @return the placesParking
	 */
	public int getPlacesParking() {
		return placesParking;
	}
	/**
	 * @param placesParking the placesParking to set
	 */
	public void setPlacesParking(int placesParking) {
		this.placesParking = placesParking;
	}
	/**
	 * @return the m2_parking
	 */
	public float getM2_parking() {
		return m2_parking;
	}
	/**
	 * @param m2_parking the m2_parking to set
	 */
	public void setM2_parking(float m2_parking) {
		this.m2_parking = m2_parking;
	}


	public String toString() {
		return "Parking \nplacesParking=" + placesParking + "\nm2_parking=" + m2_parking;
	}
	
}
